# FreeBSD elementary OS development repo

This is the [FreeBSD Ports](https://cgit.freebsd.org/ports/) collection in order to run this desktop.

> Due to bug [#275899](https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=275899), this repository will no longer receive updates.

Main branches are:

* [7.1](https://codeberg.org/olivierd/freebsd-ports-elementary/src/branch/7.1) → **default branch**
* [3rd-party-apps](https://codeberg.org/olivierd/freebsd-ports-elementary/src/branch/3rd-party-apps) → curated list of applications, which use the Granite toolkit

For complete documentation, see [wiki](../../../wiki).
