--- src/Backend/RygelConfigFile.vala.orig	2022-10-21 22:07:24 UTC
+++ src/Backend/RygelConfigFile.vala
@@ -24,7 +24,7 @@ public class Sharing.Backend.RygelConfigFile : Object 
     private string[] media_uris;
 
     construct {
-        config_filename = Path.build_filename (Environment.get_user_config_dir (), "rygel.conf");
+        config_filename = Path.build_filename ("%%PREFIX%%/etc", "rygel.conf");
         config_file = new KeyFile ();
 
         if (File.new_for_path (config_filename).query_exists ()) {
