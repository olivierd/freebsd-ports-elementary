--- src/Services/Session.vala.orig	2023-09-18 17:23:02 UTC
+++ src/Services/Session.vala
@@ -148,7 +148,7 @@ public class Notifications.Session : GLib.Object {
 
     private void create_session_file () {
         try {
-            session_file.create (FileCreateFlags.REPLACE_DESTINATION);
+            session_file.create (FileCreateFlags.PRIVATE);
         } catch (Error e) {
             warning (e.message);
         }
