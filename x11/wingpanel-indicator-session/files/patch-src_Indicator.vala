--- src/Indicator.vala.orig	2022-10-19 02:47:23 UTC
+++ src/Indicator.vala
@@ -99,10 +99,6 @@ public class Session.Indicator : Wingpanel.Indicator {
 
             main_box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
 
-            var user_settings = new Gtk.ModelButton () {
-                text = _("User Accounts Settings…")
-            };
-
             var log_out_grid = new Granite.AccelLabel (_("Log Out…"));
 
             log_out = new Gtk.ModelButton () {
@@ -151,7 +147,6 @@ public class Session.Indicator : Wingpanel.Indicator {
                 scrolled_box.add (manager.user_grid);
 
                 main_box.add (scrolled_box);
-                main_box.add (user_settings);
                 main_box.add (users_separator);
                 main_box.add (lock_screen);
                 main_box.add (log_out);
@@ -191,16 +186,6 @@ public class Session.Indicator : Wingpanel.Indicator {
 
             manager.close.connect (() => close ());
 
-            user_settings.clicked.connect (() => {
-                close ();
-
-                try {
-                    AppInfo.launch_default_for_uri ("settings://accounts", null);
-                } catch (Error e) {
-                    warning ("Failed to open user accounts settings: %s", e.message);
-                }
-            });
-
             shutdown.clicked.connect (() => {
                 show_shutdown_dialog ();
             });
@@ -263,7 +248,7 @@ public class Session.Indicator : Wingpanel.Indicator {
 
     private async void init_interfaces () {
         try {
-            system_interface = yield Bus.get_proxy (BusType.SYSTEM, "org.freedesktop.login1", "/org/freedesktop/login1");
+            system_interface = yield Bus.get_proxy (BusType.SYSTEM, "org.freedesktop.ConsoleKit", "/org/freedesktop/ConsoleKit/Manager");
             suspend.sensitive = true;
         } catch (IOError e) {
             critical ("Unable to connect to the login interface: %s", e.message);
