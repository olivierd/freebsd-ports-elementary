--- src/SettingsDaemon.vala.orig	2022-02-28 16:19:59 UTC
+++ src/SettingsDaemon.vala
@@ -51,7 +51,6 @@ public class Greeter.SettingsDaemon : Object {
 
         string[] daemons = {
             "gsd-a11y-settings",
-            "gsd-color",
             "gsd-media-keys",
             "gsd-sound",
             "gsd-power",
