--- src/MainWindow.vala.orig	2023-07-25 16:34:37 UTC
+++ src/MainWindow.vala
@@ -62,6 +62,7 @@ public class Greeter.MainWindow : Gtk.ApplicationWindo
         set_visual (get_screen ().get_rgba_visual ());
 
         guest_login_button = new Gtk.Button.with_label (_("Log in as Guest"));
+        guest_login_button.set_sensitive (false);
 
         manual_login_button = new Gtk.ToggleButton.with_label (_("Manual Login…"));
 
