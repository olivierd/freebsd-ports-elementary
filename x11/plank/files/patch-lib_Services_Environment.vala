--- lib/Services/Environment.vala.orig	2019-03-18 09:14:26 UTC
+++ lib/Services/Environment.vala
@@ -162,8 +162,6 @@ namespace Plank
 		if (result != null)
 			return XdgSessionClass.from_string (result);
 		
-		warning ("XDG_SESSION_CLASS not set in this environment!");
-		
 		return XdgSessionClass.USER;
 	}
 	
@@ -189,11 +187,11 @@ namespace Plank
 	{
 		unowned string? result;
 		
-		result = Environment.get_variable ("XDG_SESSION_TYPE");
+		result = Environment.get_variable ("GDK_BACKEND");
 		if (result != null)
 			return XdgSessionType.from_string (result);
 		
-		warning ("XDG_SESSION_TYPE not set in this environment!");
+		warning ("GDK_BACKEND not set in this environment!");
 		
 		if (Gdk.Screen.get_default () is Gdk.X11.Screen)
 			return XdgSessionType.X11;
