--- lib/Items/DockItem.vala.orig	2019-07-11 12:14:53 UTC
+++ lib/Items/DockItem.vala
@@ -318,16 +318,7 @@ namespace Plank
 				
 				load_from_launcher ();
 				break;
-			case FileMonitorEvent.MOVED:
-				if (other == null)
-					break;
-				var launcher = other.get_uri ();
-				Logger.verbose ("Launcher file '%s' moved to '%s'", f.get_uri (), launcher);
-				
-				replace_launcher (launcher);
-				
-				load_from_launcher ();
-				break;
+			case FileMonitorEvent.MOVED_OUT:
 			case FileMonitorEvent.DELETED:
 				debug ("Launcher file '%s' deleted, item is invalid now", f.get_uri ());
 				
@@ -337,6 +328,7 @@ namespace Plank
 				
 				schedule_removal_if_needed ();
 				break;
+			case FileMonitorEvent.MOVED_IN:
 			case FileMonitorEvent.CREATED:
 				debug ("Launcher file '%s' created, item is valid again", f.get_uri ());
 				
@@ -345,6 +337,31 @@ namespace Plank
 				
 				stop_removal ();
 				break;
+			case FileMonitorEvent.RENAMED:
+			case FileMonitorEvent.MOVED:
+				if (other == null)
+					break;
+				
+				var launcher = other.get_uri ();
+				
+				// Rename of launcher file to new name
+				if (launcher_exists) {
+					Logger.verbose ("Launcher file '%s' moved to '%s'", f.get_uri (), launcher);
+					
+					replace_launcher (launcher);
+					
+					load_from_launcher ();
+					break;
+				}
+				
+				// Rename of temporary file to launcher name
+				debug ("Launcher file '%s' created from rename of '%s', item is valid again", launcher, f.get_uri ());
+				
+				launcher_exists = true;
+				State &= ~ItemState.INVALID;
+				
+				stop_removal ();
+				break;
 			default:
 				break;
 			}
@@ -366,7 +383,7 @@ namespace Plank
 			try {
 				var launcher_file = File.new_for_uri (launcher);
 				launcher_exists = launcher_file.query_exists ();
-				launcher_file_monitor = launcher_file.monitor_file (FileMonitorFlags.SEND_MOVED);
+				launcher_file_monitor = launcher_file.monitor (FileMonitorFlags.NONE, null);
 				launcher_file_monitor.changed.connect (launcher_file_changed);
 			} catch {
 				warning ("Unable to watch the launcher file '%s'", launcher);
