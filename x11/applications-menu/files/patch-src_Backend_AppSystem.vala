--- src/Backend/AppSystem.vala.orig	2022-12-19 18:54:11 UTC
+++ src/Backend/AppSystem.vala
@@ -79,7 +79,7 @@ public class Slingshot.Backend.AppSystem : Object {
                 // Accessibility spec must have either the Utility or Settings category, and we display an accessibility
                 // submenu already for the ones that do not have Settings, so don't display accessibility applications here
                 excluded_categories = { "Accessibility", "System" },
-                excluded_applications = { "org.gnome.font-viewer.desktop", "org.gnome.FileRoller.desktop" }
+                excluded_applications = { "org.gnome.font-viewer.desktop", "org.gnome.FileRoller.desktop", "plank.desktop" }
             }
         );
 
@@ -207,7 +207,7 @@ public class Slingshot.Backend.AppSystem : Object {
 
         foreach (Gee.ArrayList<App> category in apps.values) {
             foreach (App app in category) {
-                if (!(app.exec in sorted_apps_execs)) {
+                if ((!(app.exec in sorted_apps_execs)) && (app.desktop_id != "plank.desktop")) {
                     sorted_apps.insert_sorted_with_data (app, sort_apps_by_name);
                     sorted_apps_execs += app.exec;
                 }
