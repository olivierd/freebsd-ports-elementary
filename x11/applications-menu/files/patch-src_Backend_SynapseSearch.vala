--- src/Backend/SynapseSearch.vala.orig	2021-11-24 22:02:41 UTC
+++ src/Backend/SynapseSearch.vala
@@ -27,7 +27,6 @@ namespace Slingshot.Backend {
             typeof (Synapse.SwitchboardPlugin),
             typeof (Synapse.SystemManagementPlugin),
             typeof (Synapse.LinkPlugin),
-            typeof (Synapse.AppcenterPlugin),
             typeof (Synapse.FileBookmarkPlugin)
         };
 
