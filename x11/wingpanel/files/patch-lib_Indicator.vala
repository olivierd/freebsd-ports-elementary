--- lib/Indicator.vala.orig	2022-01-19 09:20:07 UTC
+++ lib/Indicator.vala
@@ -21,16 +21,13 @@ public abstract class Wingpanel.Indicator : GLib.Objec
     public const string APP_LAUNCHER = "app-launcher";
     public const string SESSION = "session";
     public const string DATETIME = "datetime";
-    public const string NETWORK = "network";
     public const string MESSAGES = "messages";
     public const string SOUND = "sound";
     public const string POWER = "power";
     public const string SYNC = "sync";
     public const string PRINTER = "printer";
-    public const string BLUETOOTH = "bluetooth";
     public const string KEYBOARD = "keyboard";
     public const string NIGHT_LIGHT = "nightlight";
-    public const string PRIVACY = "privacy";
     public const string ACCESSIBILITY = "a11y";
 
     /**
