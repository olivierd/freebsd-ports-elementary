--- src/Backends/InterfaceSettings.vala.orig	2023-07-25 16:05:52 UTC
+++ src/Backends/InterfaceSettings.vala
@@ -122,9 +122,19 @@ public class SettingsDaemon.Backends.InterfaceSettings
 
             source.copy (dest, OVERWRITE | ALL_METADATA);
             // Ensure wallpaper is readable by greeter user (owner rw, others r)
-            FileUtils.chmod (dest.get_path (), 0604);
+            FileUtils.chmod (dest.get_path (), 0644);
 
-            display_manager_accounts_service.background_file = dest.get_path ();
+            // lightdm-gtk-greeter fails to load user background
+            try {
+                string abs_filename;
+
+                abs_filename = GLib.Filename.from_uri (background_settings.get_string (PICTURE_URI),
+                                                       null);
+                display_manager_accounts_service.background_file = "%s".printf (abs_filename);
+            } catch (GLib.ConvertError e) {
+                warning (e.message);
+                display_manager_accounts_service.background_file = dest.get_path ();
+            }
         } catch (IOError.IS_DIRECTORY e) {
             critical ("Migration failed %s", e.message);
             display_manager_accounts_service.background_file = "";
